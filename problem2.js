// Function lastCarDetails takes array inventory as input and returns the last car details
function lastCarDetails(inventory) {
    // Checks whether inventory is array or not
    if (Array.isArray(inventory)) {
        // To find last element in array
        const lastElement = inventory.slice(-1)[0];
        // Returns the below statement
        return `Last car is a ${lastElement.car_make} ${lastElement.car_model}`;
    }
    // Returns statement if  input is not array
    return "Please give array as input";
}
// The below statement exports function lastCarDetails which can be imported in other files
module.exports = { lastCarDetails };