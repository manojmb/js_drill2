// Function findCarYears takes array inventory as input and returns the array of car years
function findCarYears(inventory) {
    const car_year = inventory.map((car) => {
        return car.car_year;
    });
    return car_year;
}

// The below statement exports function findCarYearsAndDetails which can be imported in other files
module.exports = { findCarYears };