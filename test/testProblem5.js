// Importing findCarsBeforeYear function from problem5.js
const { findCarsBeforeYear } = require("../problem5.js");
// Importing inventory data for inventory_data.js
const { inventory } = require("./inventory_data.js");
try {
    // Cars before year to be found
    year = 2000;
    result = findCarsBeforeYear(inventory, year);
    console.log("\nProblem 5 test case:")
    console.log(`Number of car manufactured before ${year} is ${result.length}`);
    console.log(`The cars before year ${year} are :`);
    console.log(result);
} catch (e) {
    // Logs error if encounters any
    console.log(e.message);
}
