// Importing lastCarDetails from problem2.js
const { lastCarDetails } = require("../problem2.js");
// Importing inventory data for inventory_data.js
const { inventory } = require("./inventory_data.js");
// Try catch block catches if any error occured
try {
    // The result variable stores statemnet which is returned from running lastCarDetails function
    result = lastCarDetails(inventory);
    console.log("\nProblem 2 test case:")
    // Prints output on console
    console.log(result);
} catch (e) {
    // Logs error if encounters any
    console.log(e.message);
}