// Importing findCarYears from problem4.js
const { findCarYears } = require("../problem4.js")
// Importing inventory data for inventory_data.js
const { inventory } = require("./inventory_data.js");
try {
    // Result array returned from running function problem 4
    result = findCarYears(inventory);
    console.log("\nProblem 4 test case:")
    // Iterating through result array to print only years which is stored in result[0]
    for (let index = 0; index < result.length; index++) {
        console.log(result[index]);
    }
} catch (e) {
    // Logs error if encounters any
    console.log(e.message);
}