// Importing findCarById from problem1.js
const { findCarById } = require("../problem1.js");
// Importing inventory data for inventory_data.js
const { inventory } = require("./inventory_data.js");
// Executing findCarById with given test condition. This finds car with id 33
try {
    result = findCarById(inventory, 33);
    // Printing data on console
    console.log("Problem 1 test case:")
    console.log(result)
    // Irregular test case
    // result = findCarById(33, invg)
    // Printing data on console
    // console.log(result)
} catch (e) {
    // Logs error if encounters any
    console.log(e.message);
}


