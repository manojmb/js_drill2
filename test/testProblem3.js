// Importing sortAlphabetically from problem3.js
const { sortAlphabetically } = require("../problem3.js");
// Importing inventory data for inventory_data.js
const { inventory } = require("./inventory_data.js");

// Try catch block catches if any error occured
try {
    // an array result which is resulted from running function problem 3 which sorted car model name
    result = sortAlphabetically(inventory);
    console.log("\nProblem 3 test case:")
    // Iterate through result to print in console the model name of car alphabetically
    for (let i = 0; i < result.length; i++) {
        console.log(result[i]);
    }
    // Catches error if encountered any
} catch (e) {
    // Logs error if encounters any
    console.log(e.message);
}