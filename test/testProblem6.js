// Importing findBMWandAudi function from problem6.js
const { findBMWandAudi } = require("../problem6.js");
// Importing inventory data for inventory_data.js
const { inventory } = require("./inventory_data.js");
try {
    // result stores json data returned from findBMWandAudi function
    result = findBMWandAudi(inventory);
    console.log("\nProblem 6 test case:")
    console.log(result);
} catch (e) {
    // Logs error if encounters any
    console.log(e.message);
}