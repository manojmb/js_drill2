// Function findBMWandAudi takes array inventory and return array of car manufactured before 2000
function findBMWandAudi(inventory) {
    bmw_audi_array = [];
    if (Array.isArray(inventory)) {
        // Array bmw_audi_array to filter car make of audi and bmw
        const bmw_audi_array = inventory.filter((car) => {
            return car.car_make.toLowerCase() === "audi" || car.car_make.toLowerCase() === "bmw";
        });
        return JSON.stringify(bmw_audi_array);
    }
}
// The below statement exports function problem4 which can be imported in other files
module.exports = { findBMWandAudi };