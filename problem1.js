// Creating a function named findCarById which takes inventory data and id of car to be found
function findCarById(inventory, id) {
    // Checks whether inventory is array or not and id is less than 51
    if (Array.isArray(inventory) && Number(id) < 51) {
        const car_id = inventory.filter((car) => {
            return id === car.id;
        });
        return `Car ${car_id[0].id} is a ${car_id[0].car_year}  ${car_id[0].car_make} ${car_id[0].car_model}`;
    }
    // Returns below statement if above are not satisfied
    return "No such car or input is not array";
}
// The below statement exports function findCarById which can be imported in other files
module.exports = { findCarById };