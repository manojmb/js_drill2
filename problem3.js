// Function sortAlphabetically takes array inventory as input and returns the car model alphabetically
function sortAlphabetically(inventory) {
    // Checks whether inventory is array or not
    if (Array.isArray(inventory)) {
        // Use sort method to sort by car model
        inventory.sort((a, b) => {
            // Compare car a model with car b model
            if (a.car_model.toLowerCase() < b.car_model.toLowerCase()) {
                return -1;
            } else if (a.car_model.toLowerCase() > b.car_model.toLowerCase()) {
                return 1;
            }
            return 0;
        })
        const car_models = inventory.map((car) => {
            return car.car_model;
        });
        return car_models;
    }
    // Returns statement if  input is not array
    return "Please give array as input";
}

// The below statement exports function sortAlphabetically which can be imported in other files
module.exports = { sortAlphabetically };