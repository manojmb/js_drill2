// Function findCarsBeforeYear takes array inventory and return array of car manufactured before 2000
function findCarsBeforeYear(inventory, year) {
    // Checks whether inventory is array
    if (Array.isArray(inventory)) {
        // Use filter to filter out cars made before year
        const cars = inventory.filter((car) => {
            return car.car_year < year;
        });
        return cars;
    }
}
// The below statement exports function findCarsBeforeYear which can be imported in other files
module.exports = { findCarsBeforeYear };